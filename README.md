# Description
This docker-compose file can be used for a local sonarqube setup. Persistent data is mapped to the current working directory into the folders **sonar** and **postgres**. To start testing add a sonar-project.properties into the directory of you software project and make sure that all necessary sonarqube plugins are installed. E. g. Install sonarPHP if you need PHP support. That can be done with the web gui under "Administration".

Example: sonar-project.properties
```
sonar.projectKey=com.globaldatanet.myprojektkey
sonar.projectName=Project name
sonar.language=php
sonar.sources=.
sonar.scm.provide=sonarphp
```

## URL
http://localhost:9000


## Login
```
User: admin
Password: admin
```

# Install Sonar scanner
## Mac OS
```shell
$ brew install sonar-scanner
```

## Windows
Download sonar-scanner for Windows from https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/

# Usage
## Start local setup
```
$ docker-compose up
```

## Run sonar-scanner
Run it and after its done check the web gui for your project.
```
$ sonar-scanner
```
